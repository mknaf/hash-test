CC = gcc

CFLAGS = -Wall -pedantic -std=gnu99 -g

SRCS = abschluss-hash.c abschluss-test.c

OBJS = abschluss-hash.o abschluss-test.o

all: abschluss-test

abschluss-test: $(OBJS)
	$(CC) -o $@ $(CFLAGS) $^

clean:
	rm -rf abschluss-test $(OBJS)

