# About
A hash function I did for a practical exam during
my second semester.

# Original Version
The original version I handed in can be found in
the initial git commit of this repo. Later commits
are likely to be additions and changes to the original code
