#include <stdio.h>
#include <stdlib.h> /*needed for malloc*/
#include <strings.h> /*needed for bzero*/
#include <string.h> /*needed for memcpy & memcmp*/

typedef struct pair_s {
	unsigned char *key;
	char *value;
} pair;

typedef struct HashTable_s {
	pair *slot;
	int tableSize;
	int keySize;
} HashTable;

typedef struct HashIter_s {
	HashTable *hash;
	int curSlotIndex;
} HashIter;

HashTable *hash_tableNew(int tableSize, int keySize)
{

	HashTable *hash;

	if (!(hash = malloc(sizeof(HashTable)))) {
		fprintf(stderr,
			"Couldn't create new Hashtable! Not enough memory!");
		return NULL;
	}

	hash->tableSize = tableSize;
	hash->keySize = keySize;

	if (!(hash->slot = malloc(sizeof(pair)*tableSize))) {
		fprintf(stderr, "Not enough memmory!\n");
		return NULL;
	}

	int i;
	for (i = 0; i < tableSize; i++) {
		if (!(hash->slot[i].key = malloc(sizeof
					(unsigned char)*keySize))) {
			fprintf(stderr, "Not enough memory\n");
			return NULL;
		}
		hash->slot[i].key = NULL;
	}

	return hash;
}


static int hash_calcHash(unsigned char *key, int tableSize, int keySize)
{
	int hashval = 0, i;
	for (i = 0; i < keySize; i++) {
		hashval += *key % tableSize;
	}
	return hashval % tableSize;
}


int hash_insert(HashTable *hash, void *key, void *value, int valueSize)
{
	int hashval = hash_calcHash(key, hash->tableSize, hash->keySize), i = 0;

	while ((NULL != hash->slot[hashval].key) 
		&& (0 != memcmp(hash->slot[hashval].key,
			key, sizeof(unsigned char) * hash->keySize))) {

		hashval = (hashval + 1) % hash->tableSize;
		if (i > hash->tableSize) {
			return -1;
		}

		i++;
	}	

	if (NULL == hash->slot[hashval].key) {

		if(!(hash->slot[hashval].value = malloc(sizeof
					(char) * valueSize))) {
			fprintf(stderr, "Not enough memory!\n");
			return -1;
		}

		if(!(hash->slot[hashval].key = malloc(
				sizeof(unsigned char) * hash->keySize))) {
			fprintf(stderr, "Not enough memory!\n");
			free(hash->slot[hashval].value);
			return -1;
		}

		memcpy(hash->slot[hashval].key, key, sizeof
					(unsigned char) * hash->keySize);
		memcpy(hash->slot[hashval].value, value, sizeof
					(char) * valueSize);
		return 0;
	}

	if ((NULL != hash->slot[hashval].key)
			&& (0 == memcmp(hash->slot[hashval].key, key, sizeof
					(unsigned char) * hash->keySize))) {
		memcpy(hash->slot[hashval].value, value, sizeof
						(char) * valueSize);
		return 0;
	}

	return -1;
}

void hash_free(HashTable *hash)
{
	int tableSize = hash->tableSize, i;
	for (i = 0; i < tableSize; i++) {

		free(hash->slot[i].key);

		if (NULL != hash->slot[i].key) {
			free(hash->slot[i].value);
		}
	}
	free(hash->slot);
	free(hash);
}

void *hash_lookup(HashTable *hash, void *key)
{
	int hashval = hash_calcHash(key, hash->tableSize, hash->keySize);

	if ((NULL != hash->slot[hashval].key)
			&& (0 == memcmp(hash->slot[hashval].key, key, sizeof
					(unsigned char) * hash->keySize))) {	
		return hash->slot[hashval].value;
	}

	int i = 0;
	while ((NULL != hash->slot[hashval].key)
			&& (0 != memcmp(hash->slot[hashval].key, key,
				sizeof(unsigned char) * hash->keySize))) {
		hashval = (hashval + 1) % hash->tableSize;

		if (i > hash->tableSize) {
			return NULL;
		}
		i++;
	}
	return hash->slot[hashval].value;
}	

HashIter *hash_iterNew(HashTable *hash)
{
	HashIter *iter;
	if (!(iter = malloc(sizeof(HashIter)))) {
		fprintf(stderr, "Not enough memory!\n");
		return NULL;
	}
	if (iter) {
		iter->hash = hash;
		iter->curSlotIndex = 0;
		return iter;
	}
	return NULL;
}

int hash_iterNext(HashIter *iter, void **key, void **value)
{
	while ((NULL == iter->hash->slot[iter->curSlotIndex].key) &&
			(iter->curSlotIndex < iter->hash->tableSize)) {
		iter->curSlotIndex++;
	}
	
	if (iter->curSlotIndex < iter->hash->tableSize) {
		*key = iter->hash->slot[iter->curSlotIndex].key;
		*value = iter->hash->slot[iter->curSlotIndex].value;
		iter->curSlotIndex++;
		return 1;
	}
	return 0;
}

void hash_iterFree(HashIter *iter)
{
	free(iter);
}
