#include <stdio.h>
#include <string.h>
#include "abschluss-hash.h"

#define TABLESIZE 53    /* Should be prime */
#define FEATURESIZE 5   /* Size of one feature vector */
#define MAXVALSIZE 5    /* Max size of a value string */
/* You're not supposed to understand the following hack */
#define MAKESTRING(x) #x
#define EXPANDTOSTRING(x) MAKESTRING(x)
#define FEATURE_SOURCE "abschluss-features.test"


HashTable *readFeatures(char *fname)
{
	FILE *inF = fopen(fname, "r");
	HashTable *featureHash=hash_tableNew(TABLESIZE, FEATURESIZE);
	unsigned char features[FEATURESIZE];
	char value[MAXVALSIZE+1];
	int i, input;
	int inputLine=1;

	if (!inF || !featureHash) { /* Possible memory or fd leak */
		return NULL;
	}
	while (!feof(inF)) {
		for (i=0; i<FEATURESIZE; i++) {
			if (1!=fscanf(inF, "%d", &input)) {
				fprintf(stderr, "Bad number in line %d\n", inputLine);
				goto error_exit;
			}
			features[i] = (unsigned char)input;
		}
		if (1!=fscanf(inF, "%" EXPANDTOSTRING(MAXVALSIZE) "s", value) ||
				hash_insert(featureHash, features, value, strlen(value)+1)) {
			fprintf(stderr, "Bad value in line %d or table full\n", inputLine);
			goto error_exit;
		}
		inputLine++;
		fscanf(inF, "\n");  /* skip to eol for EOF detection */
	}
	fclose(inF);
	return featureHash;

error_exit:
	fclose(inF);
	hash_free(featureHash);
	return NULL;
}	


void printTable(HashTable *featureHash)
{
	HashIter *iter=hash_iterNew(featureHash);

	if (iter) {
		unsigned char *key;
		char *value;
		while (hash_iterNext(iter, (void**)&key, (void**)&value)) {
			int i;
			for (i=0; i<FEATURESIZE; i++) {
				printf("%3d ", key[i]);
			}
			printf("%s\n", value);
		}
		hash_iterFree(iter);
	}
}


void decodeMessage(unsigned char *message, int messageLength,
	HashTable *featureHash)
{
	char *value;
	int i;

	for (i=0; i<messageLength; i++, message+=FEATURESIZE) {
		value = hash_lookup(featureHash, message);
		if (value) {
			printf("%s", value);
		} else {
			printf("<UNKNOWN>");
		}
	}
	printf("\n");
}

unsigned char testMessage[] = {
	87, 210, 159, 32, 170, 38, 176, 246, 90, 104, 18, 141, 32, 28, 237, 163, 96,
	5, 240, 144, 174, 110, 25, 170, 107, 191, 229, 196, 46, 193, 19, 147, 8, 206,
	82, 18, 141, 32, 28, 237, 167, 178, 204, 104, 145, 151, 249, 23, 236, 212,
	217, 168, 96, 171, 149, 174, 110, 25, 170, 107, 134, 49, 192, 162, 35, 254,
	42, 130, 144, 53, };

int main(void)
{
	HashTable *featurehash=readFeatures(FEATURE_SOURCE);

	if (!featurehash) {
		fprintf(stderr, "Error processing %s.  Aborting.\n",
			FEATURE_SOURCE);
		return 1;
	}
	printTable(featurehash);
	decodeMessage(testMessage, 
		sizeof(testMessage)/sizeof(unsigned char)/FEATURESIZE,
		featurehash);
	return 0;
}

