#include <stdio.h>
#include <string.h>

typedef struct pair_s {
	unsigned char *key;
	char *value;
} pair;

typedef struct HashTable_s {
	pair **slot;
	int tableSize;
	int keySize;
} HashTable;

typedef struct HashIter_s {
	HashTable *hash;
	int curSlotIndex;
} HashIter;

HashTable *hash_tableNew(int tableSize, int keySize);
void hash_free(HashTable *hash);
int hash_insert(HashTable *hash, void *key, void *value, int valueSize);
void *hash_lookup(HashTable *hash, void *key);
HashIter *hash_iterNew(HashTable *hash);
void hash_iterFree(HashIter *iter);
int hash_iterNext(HashIter *iter, void **key, void **value);

